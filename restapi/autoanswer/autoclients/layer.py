from yowsup.layers.interface                           import YowInterfaceLayer, ProtocolEntityCallback
import datetime
import cherrypy

WELCOME_PAGE = """
####### ####### ##########
#        WELCOME         

# ====================   
# This is our integrated 
# autoattend             
# using whatsapp, our    
# new channel for        
# be close to you!.      

# ====================   
##########################
"""

COMMANDS_PAGE = """
####### ####### #########
#       COMMANDS         

# ====================   
# help 'Print this menu'  
# alias 'Show Your alias'
# session 'Show sesionId'
# reg-<alias> 'Register
#   ex: reg-nickname'
# ====================   
#########################
"""

class TrackSessionClient():
    def __init__(self, var):
        self.sessions={}
        self.aliases={}   #session:#number
        print "Initialization tracker %s " % var
        
    def getNumberSession(self, number):
        if number in self.sessions:
            return self.sessions[number]
        else:
            return False

    def getNumberAlias(self, number):
        if number in self.aliases:
            return self.aliases[number]
        else:
            return False

    def addAlias(self,number,alias):
        output = False
        if not number in self.aliases:
            self.aliases[number] = alias
            output = True
        else:
            print "Alias already added"
        print "Alias OK"
        return output

    def addSession(self, message):
        number = message.getFrom()
        output = False
        session = datetime.datetime.fromtimestamp(message.getTimestamp()).strftime('%d-%m-%Y')
        if not number in self.sessions:
            self.sessions[number] = session
            output = True
        else:
            print "Number already has session"
        return output

tracker = TrackSessionClient("First")

def welcome_menu(message, out):
    self = out
    number = message.getFrom()
    text = message.getBody()
    
    if tracker.addSession(message):
        message.setBody(WELCOME_PAGE)
        self.toLower(message.forward(number))
        return True
    else:
        return False

def answerHandler(message, out):
    self = out
    answer = message.getBody()
    number = message.getFrom()
    auxmsg = message
    send = ""
    var = ""
    if "help" in answer:
        auxmsg.setBody(COMMANDS_PAGE)
    elif "alias" in answer:
        if not tracker.getNumberAlias(number):
            send = "No alias, input 'reg' command"
        else:
            send = tracker.getNumberAlias(number)
        auxmsg.setBody(send)
    elif "reg-" in answer:
        var = answer.split("-")[1]
        auxmsg.setBody("Alias already is used")            
        if tracker.addAlias(number, var):
            auxmsg.setBody("Your new alias is %s" % var)
    else:
        auxmsg.setBody("No such command input 'help'")
    print auxmsg.getBody()        
    self.toLower(auxmsg.forward(number))
        
            
class AutoLayer(YowInterfaceLayer):
                    
    @ProtocolEntityCallback("message")
    def onMessage(self, messageProtocolEntity):
        number = messageProtocolEntity.getFrom()
        if messageProtocolEntity.getType() == 'text':
            self.onTextMessage(messageProtocolEntity)
        elif messageProtocolEntity.getType() == 'media':
            self.onMediaMessage(messageProtocolEntity)
        welcome_menu(messageProtocolEntity, self)
        answerHandler(messageProtocolEntity, self)
        self.toLower(messageProtocolEntity.ack())
        self.toLower(messageProtocolEntity.ack(True))
        
     
    @ProtocolEntityCallback("receipt")
    def onReceipt(self, entity):
        self.toLower(entity.ack())

    def onTextMessage(self,messageProtocolEntity):
        # just print info
        print("Echoing More to More %s to %s" % (messageProtocolEntity.getBody(), messageProtocolEntity.getFrom(False)))

    def onMediaMessage(self, messageProtocolEntity):
        # just print info
        if messageProtocolEntity.getMediaType() == "image":
            print("Echoing image %s to %s" % (messageProtocolEntity.url, messageProtocolEntity.getFrom(False)))

        elif messageProtocolEntity.getMediaType() == "location":
            print("Echoing location (%s, %s) to %s" % (messageProtocolEntity.getLatitude(), messageProtocolEntity.getLongitude(), messageProtocolEntity.getFrom(False)))

        elif messageProtocolEntity.getMediaType() == "vcard":
            print("Echoing vcard (%s, %s) to %s" % (messageProtocolEntity.getName(), messageProtocolEntity.getCardData(), messageProtocolEntity.getFrom(False)))


