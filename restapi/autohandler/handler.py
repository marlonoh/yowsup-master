# -*- coding: utf-8 -*-
from .util import *
from restproxy.web import *
import datetime

URL_BY_CELLPHONE = "http://192.168.122.1:8080/getLoanByCellphone"
URL_BY_USERID = "http://192.168.122.1:8080/getLoanByUserid"
URL = "http://192.168.122.1:8080/

class TrackerSession(object):
    def __init__(self, sessiontime=600):
        self.aliases = {}
        self.sessions = {}
        self.sessiontime = sessiontime

    def isConnected(self, client):
        if client in self.sessions:
            if self.refreshSession(client):
                return true
        return false

    def addSession(self, client):
        if self.isConnected(client):
            self.sessions[client] = createSession

    def createSessionId(self, client):
        return datetime.datetime.now()

    def refreshSession(self, client):
        if client in sessions:
            now = datetime.datetime.now()
            diff = now - session[client]
            if diff < self.sessiontime:
                sessions[client] = now
                return true
        return false


class MessageHandler(object):
    def __init__(self, api, message, tracker, url=URL):
        self.api = api
        self.message = message
        self.auxmsg = message
        self.client = message.getFrom()
        self.request = WebRequests()
        self.messageStr = MessagesStr()
        self.messageCli = MessagesCli()
        self.url = url

    def refreshMsg(self):
        self.message = self.auxmsg

    def sendClientResponse(self, clientResponse):
        self.message.setBody = clientResponse
        self.api.sendMessage(self.message, self.client)
        self.refreshMsg()

    def requestBalance(self):
        dics = self.request.postJson2url(
            URL_BY_CELLPHONE, {"cellphone": self.client})
        if not "null" in dics:
            self.sendClientResponse(
                self.messageStr.addline("Ur balance is:" + dics["balance__c"]))
        self.sendClientResponse(
                self.messageStr.addline("Ur cellphone is not registered"))

    def requestBalanceTest(self):
        dics = self.request.postJson2url(
            URL_BY_CELLPHONE, {"cellphone": "573147920407"})
        if not "null" in dics:
            self.sendClientResponse(
                self.messageStr.addline("Ur balance is:" + dics["balance__c"]))
        self.sendClientResponse(
                self.messageStr.addline("Ur cellphone is not registered"))

    def requestMenu(self):
        commands = ["Get Loan Balance", "Get Loan BalanceTest",
             "Print this menu", "Get Next Payment Date",
             "Get Next Payment Amount", "Get All Loan Info"]
        self.sendClientResponse(self.messageStr.menu(commands))

    def defaultHandler(self):
        self.sendClientResponse(self.messageStr.error(
            "No such command, try with option 911 for help"))

    def sendMessages(self, msg, types="default"):
        switch={
            "info": self.sendClientResponse(self.messageStr.info(
            msg))
            "error": self.sendClientResponse(self.messageStr.error(
            msg))
            "default": self.sendClientResponse(self.messageStr.addline(
            msg))
            }
        switch[types.lower()]
       
    def welcomeMessage(self):
        self.sendClientResponse(self.messageStr.addline(
            "Welcome to our new communication channel"))

    def processClientRequest(self, option):
        switcher = {
            1: requestBalance,
            2: requestBalanceTest,
            3: requestMenu,
            911: requestMenu
            }
        func = switcher.get(option, defaultHandler)
        return func()


