# -*- coding: utf-8 -*-
import requests
import json

##
# This class is an object definition
#
class Loan (object):
    def __init__(self, userid, cellphone, balance, nextPaymentDate,
    nextPaymentAmount):
        self.userid = userid
        self.cellphone = cellphone
        self.balance = balance
        self.nextPaymentDate = nextPaymentDate
        self.nextPaymentAmount = nextPaymentAmount


##
# This class allow to send post and get request to get dicc response
# If response status code is 200 return dictionary, else return Error code
class WebRequests(object):
    def __init__(self):
        super(WebRequests, self).__init__()

    def posturl(self, url, params):
        r = requests.post(url, params)
        if r.status_code == 200:
            dicc = json.loads(r.text)
        else:
            dicc['status'] = 'Error %s' % r.status_code
        return dicc

    def geturl(self, url, params):
        r = requests.get(url, params)
        if r.status_code == 200:
            dicc = json.loads(r.text)
        else:
            dicc['status'] = 'Error %s' % r.status_code
        return dicc

    def getJson2url(self, url, jsonParam):
        r = requests.get(url, jsonParam)
        if r.status_code == 200:
            dicc = json.loads(r.text)
        else:
            dicc['status'] = 'Error %s' % r.status_code
        return dicc

    def postJson2url(self, url, jsonParam):
        r = requests.post(url, json=jsonParam)
        if r.status_code == 200:
            dicc = json.loads(r.text)
        else:
            dicc['status'] = 'Error %s' % r.status_code
        return dicc

    def object_decoder(obj):
        if '__type__' in obj and obj['__type__'] == 'Loan__c':
            return Loan(obj['id__c'], obj['cellphone__c'], obj['balance__c'],
        obj['next_Payment_Date__c'], obj['next_Payment_Amount__c'])
        return obj


##
# Usage:
# json.loads('{"__type__": "User", "name": "John Smith",
# "username": "jsmith"}', object_hook=object_decoder)
# returns Loan Object
def object_decoder(obj):
    if '__type__' in obj and obj['__type__'] == 'Loan__c':
        return Loan(obj['id__c'], obj['cellphone__c'], obj['balance__c'],
    obj['next_Payment_Date__c'], obj['next_Payment_Amount__c'])
    return obj


class MessagesCli(object):
    def __init__(self, charact='='):
        self.charact = charact

    def error(self, msg, size=30):
        strs = " " + msg + " "
        print " Error ".center(size, self.charact)
        print strs.center(size, self.charact)
        print " ".center(size, self.charact)

    def addline(self, msg, size=30):
        strs = " " + msg + " "
        print strs.center(size, self.charact)

    def endline(self,msg=" ", size=30):
        print msg.center(size, self.charact)

    def info(self, msg, size=30):
        strs = " " + msg + " "
        print " Info ".center(size, self.charact)
        print strs.center(size, self.charact)
        print " ".center(size, self.charact)

    def menu(self, listcmds, size=30):
        print " MENU ".center(size, self.charact)
        i = 0
        for cmd in listcmds:
            strs = str(i + 1) + "). " + cmd
            self.addline(strs)
            i += 1
        self.addline("Escoja una opcion o introduzca help")
        self.endline()


class MessagesStr(object):
    def __init__(self, charact='='):
        self.charact = charact

    def error(self, msg, size=30):
        strs = " " + msg + " "
        err = " Error ".center(size, self.charact)
        err += "\n" + strs.center(size, self.charact)
        err += "\n" + " ".center(size, self.charact)
        return err + "\n"

    def addline(self, msg, size=30):
        strs = " " + msg + " "
        pr = strs.center(size, self.charact)
        return pr + "\n"

    def endline(self, msg=" ", size=30):
        return msg.center(size, self.charact) + "\n"

    def info(self, msg, size=30):
        strs = " " + msg + " "
        inf = " Info ".center(size, self.charact)
        inf += "\n" + strs.center(size, self.charact)
        inf += "\n" + " ".center(size, self.charact)
        return inf + "\n"

    def menu(self, listcmds, size=30):
        mn = " MENU ".center(size, self.charact)
        i = 0
        for cmd in listcmds:
            strs = str(i + 1) + "). " + cmd
            mn += self.addline(strs)
            i += 1
        mn += self.addline("Escoja una opcion o introduzca help")
        mn += self.endline()




