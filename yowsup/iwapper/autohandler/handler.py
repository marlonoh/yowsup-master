# -*- coding: utf-8 -*-
from .util import *
from restproxy.web import *
import datetime

URL_BY_CELLPHONE = "http://192.168.122.1:8080/getLoanByCellphone"
URL_BY_USERID = "http://192.168.122.1:8080/getLoanByUserid"
URL = "http://localhost:9090/

class TrackerSession(object):
    def __init__(self, sessiontime=600):
        self.aliases = {}
        self.sessions = {}
        self.sessiontime = sessiontime

    def isConnected(self, client):
        if client in self.sessions:
            if self.refreshSession(client):
                return True
        return False

    def addSession(self, client):
        if not self.isConnected(client):
            self.sessions[client] = datetime.datetime.now()
            return True
        return False

    def createSessionId(self, client):
        return datetime.datetime.now()

    def refreshSession(self, client):
        if client in sessions:
            now = datetime.datetime.now()
            diff = now - session[client]
            diff = (diff.seconds//60)%60
            if diff < self.sessiontime:
                sessions[client] = now
                return True
            else:
                sessions.pop(client)
        return False

class TrackSessionClient():
    def __init__(self, var):
        self.sessions={}
        self.aliases={}   #session:#number
        print "Initialization tracker %s " % var
        
    def getNumberSession(self, number):
        if number in self.sessions:
            return self.sessions[number]
        else:
            return False

    def getNumberAlias(self, number):
        if number in self.aliases:
            return self.aliases[number]
        else:
            return False

    def addAlias(self,number,alias):
        output = False
        if not number in self.aliases:
            self.aliases[number] = alias
            output = True
        else:
            print "Alias already added"
        print "Alias OK"
        return output

    def addSession(self, message):
        number = message.getFrom()
        output = False
        session = datetime.datetime.fromtimestamp(message.getTimestamp()).strftime('%d-%m-%Y')
        if not number in self.sessions:
            self.sessions[number] = session
            output = True
        else:
            print "Number already has session"
        return output



class MessageHandler(object):
    def __init__(self, api, message, tracker, url=URL):
        self.api = api
        self.message = message
        self.auxmsg = message
        self.client = message.getFrom()
        self.request = WebRequests()
        self.messageStr = MessagesStr()
        self.messageCli = MessagesCli()
        self.url = url

    def refreshMsg(self):
        self.message = self.auxmsg

    def sendClientResponse(self, clientResponse):
        self.message.setBody(clientResponse)
        self.api.toLower(self.message.forward(number))
        #sending(api, message)
        self.refreshMsg()

    def requestBalance(self):
        dics = self.request.postJson2url(
            self.url + "/getLoanByCellphone", {"cellphone": self.client})
        if not "null" in dics:
            self.sendClientResponse(
                self.messageStr.addline("Ur balance is:" + dics["balance__c"]))
        self.sendClientResponse(
                self.messageStr.addline("Ur cellphone is not registered"))

    def requestBalanceTest(self):
        dics = self.request.postJson2url(
            URL_BY_CELLPHONE, {"cellphone": "573147920407"})
        if not "null" in dics:
            self.sendClientResponse(
                self.messageStr.addline("Ur balance is:" + dics["balance__c"]))
        self.sendClientResponse(
                self.messageStr.addline("Ur cellphone is not registered"))

    def requestMenu(self):
        commands = ["balance: Get Loan Balance", "test: Get Loan BalanceTest",
             "help: Print this menu", "Get Next Payment Date",
             "Get Next Payment Amount", "Get All Loan Info"]
        self.sendClientResponse(self.messageStr.menu(commands))
        
    def defaultHandler(self):
        self.sendClientResponse(self.messageStr.error(
            "No such command, try with option 911 for help"))

    def sendMsg(self, msg, types="default"):
        switch={
            "info": self.sendClientResponse(self.messageStr.info(
            msg))
            "error": self.sendClientResponse(self.messageStr.error(
            msg))
            "default": self.sendClientResponse(msg)
            }
        switch[types.lower()]
       
    def welcomeMessage(self, msg):
        self.sendClientResponse(self.messageStr.addline(msg))

    def processClientRequest(self, option):
        switcher = {
            "balance": requestBalance,
            "test": requestBalanceTest,
            "help": requestMenu,
            }
        func = switcher.get(option, defaultHandler)
        return func()


def sending(api, message):
    self = api
    number = message.getFrom()
    self.toLower(message.forward(number))
    
