# -*- coding: utf-8 -*-
import requests
import json
WELCOME_PAGE = """
####### ####### ##########
#        WELCOME         

# ====================   
# This is our integrated 
# autoattend             
# using whatsapp, our    
# new channel for        
# be close to you!.      

# ====================   
##########################
"""

##
# This class is a POJO Loan Object
#
class Loan (object):
    def __init__(self, balance, nextPaymentDate,
    nextPaymentAmount, name, cellphone=""):
        self.cellphone = cellphone
        self.balance = balance
        self.nextPaymentDate = nextPaymentDate
        self.nextPaymentAmount = nextPaymentAmount
        self.name = name

##
# This class is a POJO Customer Object
#
class Customer (object):
    def __init__(self, customerId, cellphone,
                 email, objectid, firstname="New Customer"):
        self.cellphone = cellphone
        self.customerId = customerId
        self.email = email
        self.firstname = firstname
        self.id = objectid

class ServiceLog (object):
    def __init__(self, customerId, name,
                 logType):
        self.customerId = customerId
        self.name = name
        self.logType = logType
        self.id = objectid

class MyCustomerInfo(object):
    def __init__(self, customer, loan):
        self.customer = customer
        self.loans = loan

##
# This class allow to send post and get request to get dicc response
# If response status code is 200 return dictionary, else return Error code
class WebRequests(object):
    def __init__(self):
        super(WebRequests, self).__init__()

    def posturl(self, url, params):
        dicc = {}
        try:
            r = requests.post(url, params)
            dicc['status'] = "200"
            if r.status_code == 200:
                dicc = json.loads(r.text)
            else:
                dicc['status'] = 'Error %s' % r.status_code
        except requests.exceptions.ConnectTimeout as e:
            dicc['status'] = e
        except requests.exceptions.RequestException as e:
            dicc['status'] = e
        return dicc

    def geturl(self, url, params):
        dicc = {}
        try:
            r = requests.get(url, params)
            if r.status_code == 200:
                dicc = json.loads(r.text)
                dicc['status'] = "200"
            else:
                dicc['status'] = 'Error %s' % r.status_code
        except requests.exceptions.ConnectTimeout as e:
            dicc['status'] = e
        except requests.exceptions.RequestException as e:
            dicc['status'] = e
        return dicc

    def getJson2url(self, url, jsonParam):
        dicc = {}
        try:
            r = requests.get(url, jsonParam)
            if r.status_code == 200:
                dicc = json.loads(r.text)
                dicc['status'] = "200"
            else:
                dicc['status'] = 'Error %s' % r.status_code
        except requests.exceptions.ConnectTimeout as e:
            dicc['status'] = e
        except requests.exceptions.RequestException as e:
            dicc['status'] = e
        return dicc

    def getLoanObject(self, obj):
        return self.object_decoder(obj)
    
    def postJson2url(self, url, jsonParam):
        dicc = {}
        try:
            r = requests.post(url, json=jsonParam)            
            if r.status_code == 200:
                return json.loads(r.text)
            else:
                dicc['status'] = 'Error %s' % r.status_code
        except requests.exceptions.ConnectTimeout as e:
            dicc['status'] = e
        except requests.exceptions.RequestException as e:
            dicc['status'] = e
        return dicc

    def jsonToObject(self, objs, mytype):
        if "loan" in mytype.lower():
            l = []
            for obj in objs:
                l.append(Loan(obj['balance__c'],
                              obj['next_Payment_Date__c'],
                              obj['next_Payment_Amount__c'],
                              obj['name']
                              )
                         )
            return l
        if "customer" in mytype.lower():
            return Customer(objs['id_Customer__c'],
                            objs['cellphone__c'],
                            objs['email__c'],
                            objs['id'],
                            objs['first_Name__c']
                            )
        return obj


##
# Usage:
# json.loads('{"__type__": "User", "name": "John Smith",
# "username": "jsmith"}', object_hook=object_decoder)
# returns Loan Object
def object_decoder(obj,mytype):
    if "Loan" in mytype.lower():
       return Loan(obj['id__c'], obj['cellphone__c'], obj['balance__c'],
                obj['next_Payment_Date__c'], obj['next_Payment_Amount__c'])
    if "Customer" in mytype.lower():
        return Customer(obj['id_Customer__c'], obj['cellphone__c'], obj['email__c'],
                obj['Name'], obj['first_Name__c'])


class MessagesCli(object):
    def __init__(self, charact='='):
        self.charact = charact

    def error(self, msg, size=30):
        strs = " " + msg + " "
        print " Error ".center(size, self.charact)
        print strs.center(size, self.charact)
        print " ".center(size, self.charact)

    def addline(self, msg, size=30):
        strs = " " + msg + " "
        print strs.center(size, self.charact)

    def endline(self,msg=" ", size=30):
        print msg.center(size, self.charact)

    def info(self, msg, size=30):
        strs = " " + msg + " "
        print " Info ".center(size, self.charact)
        print strs.center(size, self.charact)
        print " ".center(size, self.charact)

    def menu(self, listcmds, size=30):
        print " MENU ".center(size, self.charact)
        i = 0
        for cmd in listcmds:
            strs = str(i + 1) + "). " + cmd
            self.addline(strs)
            i += 1
        self.addline("Escoja una opcion o introduzca help")
        self.endline()


class MessagesStr(object):
    MESSAGE_FORMAT = "[{CMD}]: {MESSAGE_CMD}"
    BUILD_MESSAGE = "[{TITTLE}]: {PARAMS}"
    def __init__(self, charact='#'):
        self.charact = charact

    def error(self, msg, size=25):
        strs = " " + msg + " "
        err = " Error ".center(size, self.charact)
        err += "\n" + strs.center(size, self.charact)
        err += "\n" + " ".center(size, self.charact)
        return err + "\n"

    def addline(self, msg, size=25):
        strs = " " + msg + " "
        pr = strs.center(size, self.charact)
        return pr + "\n"

    def buildmsg(self,tittle, msg, status="NOTICE", size=25):
        strs = " "+status+" "
        strs = strs.center(size,self.charact)
        string = self.__class__.BUILD_MESSAGE.format(
                TITTLE = tittle,
                PARAMS = msg                
                )
        strs += "\n" + string
        strs += "\n" + "  ".center(size, self.charact)
        return strs + "\n"

    def endline(self, msg=" ", size=25):
        return msg.center(size, self.charact) + "\n"

    def info(self, msg, size=25):
        strs = " " + msg + " "
        inf = " Info ".center(size, self.charact)
        inf += "\n" + strs.center(size, self.charact)
        inf += "\n" + " ".center(size, self.charact)
        return inf + "\n"

    def menu(self, listcmds, size=25):
        mn = " MENU ".center(size, self.charact)
        i = 0
        for cmd in listcmds:
            strs = str(i + 1) + "). " + cmd
            mn += self.addline(strs)
            i += 1
        mn += self.addline("Escoja una opcion o introduzca help")
        mn += self.endline()

    def menubuilder(self, listcmds, size=25):
        strs = " " + msg + " "
        err = " Error ".center(size, self.charact)
        for cmd in listcmds:
            err += "\n" + cmd + "\n"
        err += " ".center(size, self.charact)
        return err + "\n"

    def helpcmds(self, listcmds, size=25):
        err = " COMMANDS ".center(size, self.charact)
        for cmd in listcmds:
            string = self.__class__.MESSAGE_FORMAT.format(
                CMD = cmd.split(":")[0],
                MESSAGE_CMD = cmd.split(":")[1]                
                )
            err += "\n" + self.charact + " " + string
        err += "\n" + " ".center(size, self.charact)
        return err + "\n"
        
        




