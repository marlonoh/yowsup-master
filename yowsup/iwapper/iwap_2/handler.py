# -*- coding: utf-8 -*-
from .util import *
#from restproxy.web import *
import datetime


URL_BY_CELLPHONE = "http://192.168.122.1:8080/getLoanByCellphone"
URL_BY_USERID = "http://192.168.122.1:8080/getLoanByUserid"
URL = "http://localhost:9090/"

WELCOME_PAGE = """
####### ####### ##########
# WELCOME TO IWAP
# Dear {NAME}         
# ====================   
# This is our integrated 
# autoattend system             
# using whatsapp; our    
# new channel for close
# us to you!.      
# ====================   
##########################
"""

INFO_FORM= """
####### ####### ##########
# {NAME}'S INFORMATION
# ====================   
# CC: {ID}
# Email: {EMAIL}             
# Cellphone: {CELL}    
# ====================   
##########################
"""

LOG_TITTLE_FORM = """{TYPE} - {CELL}"""
class TrackerSession(object):
    def __init__(self, sessiontime=600):
        self.allinfo ={}
        self.aliases = {}
        self.sessions = {}
        self.sessiontime = sessiontime
        self.loans={}
        self.notregs={}

    def isConnected(self, client):
        if client in self.sessions:
            if self.refreshSession(client):
                return True
        return False

    def disconnect(self, client):
        if client in self.sessions:
            self.sessions.pop(client)
    
    def addSession(self, client):
        if not self.isConnected(client):
            self.sessions[client] = datetime.datetime.now()
            return True
        return False

    def addMyCustomerInfo(self, mycustomer, client):
        self.allinfo[client] = mycustomer

    def addInfoCustomer(self, client, loan, customer):
        if self.isConnected(client):
            self.loans[client] = loan
            self.aliases[client] = customer
            return True
        return False

    def createSessionId(self, client):
        return datetime.datetime.now()

    def refreshSession(self, client):
        if client in self.sessions:
            now = datetime.datetime.now()
            diff = now - self.sessions[client]
            diff = (diff.seconds//60)%60
            if diff < self.sessiontime:
                self.sessions[client] = now
                return True
            else:
                self.sessions.pop(client)
        return False

class MessageHandler(object):

    def __init__(self, api, message, tracker, url=URL):
        self.api = api
        self.message = message
        self.auxmsg = message
        self.client = message.getFrom()
        self.request = WebRequests()
        self.messageStr = MessagesStr()
        self.messageCli = MessagesCli()
        self.url = url
        if self.client in tracker.allinfo:
            self.info = tracker.allinfo[self.client]
            self.loan = self.info.loan
            self.customer = self.info.customer
        else:
            self.loan = None
            self.customer = None
  
    def refreshMsg(self):
        self.message = self.auxmsg

    def sendLog(self, logType, customerId, info=""):
        if not customerId:
            customerId = self.customer.id
        tittle = LOG_TITTLE_FORM.format(
            TYPE=logType,
            CELL=self.customer.cellphone.split("@")[0]
            )
        dics = self.request.postJson2url(
            self.url + "/loggingService",
            {"logname": tittle, "logtype": logType, "customerid": customerId}
            )
        return dics
  
    def sendClientResponse(self, clientResponse):
        number = self.client
        self.message.setBody(clientResponse)
        self.api.toLower(self.message.forward(number))
        self.refreshMsg()

    def requestLoanObject(self):
        dics = self.request.postJson2url(
            self.url + "/getLoanByCellphone", {"cellphone": self.client})
        return self.request.jsonToObject(dics, "loan")

    def requestCustomerObject(self):
        dics = self.request.postJson2url(
            self.url + "/getCustomerByCellphone", {"cellphone": self.client})
        return self.request.jsonToObject(dics, "customer")
        
    
    def requestBalance(self):
        self.sendLog("Balance",
                     self.customer.id
                     )
        if self.loan.balance:
            self.sendClientResponse(
                self.messageStr.buildmsg("Your Balance", str(self.info.loan.balance)))
        else:
            self.sendClientResponse(
                self.messageStr.buildmsg("Dear Customer",
                                         "Your cellphone is NOT in our database", "NOT REGISTERED"))
            
    def requestNextDate(self):
        self.sendLog("Date",
                     self.customer.id
                     )
        if self.loan.nextPaymentDate:
            self.sendClientResponse(
                self.messageStr.buildmsg("Next Payment Date",str(self.info.loan.nextPaymentDate)))
        else:
            self.sendClientResponse(
                self.messageStr.buildmsg("Dear Customer",
                                         "Your cellphone is NOT in our database", "NOT REGISTERED"))
            
    def requestNextAmount(self):
        self.sendLog("Amount",
                     self.customer.id
                     )
        if self.loan.nextPaymentAmount:
            self.sendClientResponse(
                self.messageStr.buildmsg("Next Payment Amount",str(self.info.loan.nextPaymentAmount)))
        else:
            self.sendClientResponse(
                self.messageStr.buildmsg("Dear Customer",
                                         "Your cellphone is NOT in our database", "NOT REGISTERED"))

    def requestMyInfo(self):
        if self.customer.firstname:
            string = INFO_FORM.format(NAME=self.customer.firstname,
                                 ID=self.customer.customerId,
                                 EMAIL=self.customer.email,
                                 CELL=self.customer.cellphone)
            self.sendClientResponse(string)
        else:
            self.sendClientResponse(
                self.messageStr.buildmsg("Dear Customer",
                                         "Your cellphone is NOT in our database", "NOT REGISTERED"))
            
    def requestMenu(self):
        self.customer = self.requestCustomerObject()
        if self.customer.firstname:
            msg = WELCOME_PAGE.format(NAME=self.customer.firstname)
            self.sendClientResponse(self.messageStr.addline(msg))
            self.sendLog("Login",
                     self.customer.id
                     )
            return True
        self.sendClientResponse(
            self.messageStr.buildmsg("Dear Customer",
                                     "Your cellphone is NOT in our database", "NOT REGISTERED"))
        return False
       
    def requestHelp(self):
        commands = [
            "balance: Get Loan Balance",
            "test: Get Loan BalanceTest",
            "help: Print this menu",
            "nextdate: Get Next Payment Date",
            "nextamount: Get Next Payment Amount",
            "myinfo: Get Some Info Customer",
            "escape: Close session" 
            ]
        self.sendClientResponse(self.messageStr.helpcmds(commands))
       
    def processClientRequest(self, option):
        switcher = {
            "balance": self.requestBalance,
            "help": self.requestHelp,
            "test": self.sendLogTest,
            "myinfo": self.requestMyInfo,
            "nextdate": self.requestNextDate,
            "nextamount": self.requestNextAmount,
            }
        func = switcher.get(option.lower(), self.defaultHandler)
        return func()
    
    def defaultHandler(self):
        self.sendClientResponse(self.messageStr.error(
            "No such command, try using 'help'"))

    def sendMsg(self, msg, types="default"):
        self.sendClientResponse(msg)

    def sendLogTest(self):
        self.sendLog(self.client.split("@")[0] +" - "+self.customer.firstname,
                     "Balance", self.customer.id)
        
    def setInfoCustomer(self, allcustomer):
        self.customer = allcustomer.customer
        self.loan = allcustomer.loan

    def getCustomerObject(self):
        return self.customer

    

class MyCustomerInfo(object):
    def __init__(self, customer, loan):
        self.customer = customer
        self.loan = loan

def sending(api, message):
    self = api
    number = message.getFrom()
    self.toLower(message.forward(number))
    
